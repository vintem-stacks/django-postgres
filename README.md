# Django with Postgres Docker Stack

[pt-br]

Este é um '*boilerplate*' de uma *stack* para desenvolvimento de
aplicações baseadas em Django, com banco de dados Postgres, embarcados
em contêineres Docker.

## Dependências

- Poetry
- Docker
- Docker-compose

## Como utilizar

- Altere os valores das variáveis de ambiente, sobretudo aquelas
relacionadas a passwords/secretes, no arquivo [.env](.env)  

- Suba o cluster docker

```shell
$ docker-compose up
```